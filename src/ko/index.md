---
title: 홈
layout: layout/landing.njk

pagination:
  data: collections.posts_ko
  size: 10
  reverse: true
  alias: posts

slogan: 저희는 첨단기술만 고집합니다!
hero_button_link: /ko/pages/direction
description: 윤택이엔지, Unitek Eng.
keywords: ["Hydraulics", "Health care", "NDT"]
---

# 고객의 성공을 위해 더 나은 제품과 솔루션을 제공합니다.


<div class="flex space-x-4 my-5 ">
  <div class="rounded overflow-hidden hover:shadow-lg w-1/3 ">
    <a class="no-underline hover:no-underline" href="{{'/ko/products/' | url }}">
      <img class="full before:bg-black/50" src="{{'/images/biz-hydraulics.jpg' | url }}">
      <figcaption class="absolute -mt-24 text-white px-4 w-2/5 ">
        <div class="text-xl" style="text-shadow: 1px 1px 5px black;">Tough, long-lasting</div>
      </figcaption>
      <div class="px-6 py-4">
        <div class="text-xl text-blue-800">Hydraulics
        </div>
      </div>
    </a>
  </div>

  <div class="rounded overflow-hidden hover:shadow-lg w-1/3 ">
    <a class="no-underline hover:no-underline" href="{{'/ko/products/' | url }}">
      <img class="full before:bg-black/50" src="{{'/images/biz-healthcare.jpg' | url }}">
      <figcaption class="absolute -mt-24 text-white px-4 w-2/5 ">
        <div class="text-xl" style="text-shadow: 1px 1px 5px black;">Care for your loved ones</div>
      </figcaption>
      <div class="px-6 py-4">
        <div class="text-xl text-blue-800">Health care
        </div>
      </div>
    </a>
  </div>

  <div class="rounded overflow-hidden hover:shadow-lg w-1/3 ">
    <a class="no-underline hover:no-underline" href="{{'http://www.unitek-ndt.com' }}">
      <img class="full before:bg-black/50" src="{{'/images/biz-ndt.jpg' | url }}">
      <figcaption class="absolute -mt-24 text-white px-4 w-1/5 ">
        <div class="text-xl" style="text-shadow: 1px 1px 5px black;">Highest quality <br/>Inspection system</div>
      </figcaption>
      <div class="px-6 py-4">
        <div class="text-xl text-blue-800">NDT
        </div>
      </div>
    </a>
  </div>

</div>
