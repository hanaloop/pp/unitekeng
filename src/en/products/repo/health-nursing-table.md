---
title: PRESSALIT / Nursing Table

product:
  category: Health care
  brand: PRESSALIT
  name: Nursing table
  description: Table for infant nursery, considering safety and hygiene.
  applications: ["욕실: 샤워의자 및 팔걸이, 변기 팔걸이, 세면대 및 팔걸이, 샤워걸이", "부엌: 상하이동 싱크대 및 상부 찬장"]

meta:
  update: 2021-08-10
  logo: fundamenty-logo.png
  keywords: ["Eleventy", "SSG"]
  author: Young-Suk Ahn Park
---


## {{ product.name }}
Pressalit Care사의 **Nursing table은** 영유아의 안전 및 위생을 고려하여 제작한 제품으로 특히 보호자가 영유아의 용변 체크 및 기저귀를 교환할 때에 허리와 척추의 부상을 예방하기 위해 높낮이를 전동식으로 조절할 수 있도록 설계된 제품입니다. 개수대가 부착되어 있는 제품과 접이식 제품, 자립식 제품 등 다양한 형태의 제품 중 선택하실 수 있어 설치할 공간에 맞춰 보다 편리하게 사용하실 수 있습니다.

* 적용분야: 아동전문병원, 병원 내 신생아실, 신생아중환자실, 산후조리원, 각종 공공시설의 수유실 등


[TBD: Actual Content]
