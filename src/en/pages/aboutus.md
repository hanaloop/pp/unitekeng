---
title: About Us
---

# Unitek Engineering


## Business Principles

* We listen carefully to our customers
* We deliver our promise

## Mission

We provide highest quality products and solutions, and strive for the success of our customers.

## Core Values

* Trust
* Teamwork
* Professionalism

## Slogan

We are **Uni(潤)+Tek(澤)**
We make a brighter world together.


## Information

* Company Name   : (주)윤택이엔지
* Established on : April 25th, 2000 (1979년 7월 1일 설립된 (주)윤택사로부터 분사)
* Addresss : Seoul Kangnam-ku Teheran-ro 108, 22, Seokyung building 3rd floor (Daechi-dong 1000-3)
* Phone : 02-567-0888
* Fax   : 02-3453-1177
