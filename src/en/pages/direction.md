---
title: 차자오시는 길
---

<br />

## Direction

<img src="{{ "/images/map.png" | url }}" width="100%" />

* Driving : Samsung station -> Teheran-ro -> turn right on 230m.
* Subway : Line 2, Samsung Station exit #1
* Bus    : Samsung station 143, 146, 401, 2413, 3422, 4318, 4319, 4419, 41, 11-3, 917, 9507 / 광역 9407,9414,6900

